class Vm                            # default virtual machine settings
  Basebox   = "debian-LAMP-2012-03-29"   # basebox (Can be overridden)
  Box_url   = "http://ergonlogic.com/files/boxes/debian-LAMP-current.box" # (Can be overridden)
  Gui       = false                 # start VM with GUI? Useful for loading CD/DVD ISOs
  Memory    = 512                   # default VM memory (Can be overridden)
  Manifests = "manifests"           # puppet manifests folder name
  Modules   = "modules"             # puppet modules folder name
  Verbose   = false                 # make output verbose?
  Debug     = false                 # output debug info?
  Options   = ""                    # options to pass to Puppet
  Domain    = "aegir.local"
end

class Hm                            # settings for our Aegir hostmaster machine
  Shortname = "hm"                  # Vagrant name (also used for manifest name, e.g., hm.pp)
  Vmname    = "Aegir"               # VirtualBox name
  Aegir_root = "/var/aegir"
  Aegir_user = "aegir"
end

###############################################################################
# WARNING: We haven't tested Hostslaves for a LONG time. Patches welcome.
###############################################################################

class Hs                            # settings for our Aegir hostslave machine(s)
  Count     = 0                     # number of hostslaves to create (will be used as a suffix to Shortname, Hostname & Vmname)
  Shortname = "hs"                  # Vagrant name (also used for manifest name, e.g., hs.pp)
  Vmname    = "Cluster"             # VirtualBox name
end
