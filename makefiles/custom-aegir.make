api = 2
core = 6.x

;; Drupal & Hostmaster

projects[drupal][type] = "core"


; Contrib modules
projects[admin_menu][version] = "1.8"
projects[openidadmin][version] = "1.2"
projects[install_profile_api][version] = "2.1"
projects[jquery_ui][version] = "1.4"
projects[modalframe][version] = "1.6"

; These are contrib modules, but come under the Aegir 'umbrella' of control.
projects[hosting_platform_pathauto][version] = "2.0-beta1"

; Libraries
libraries[jquery_ui][download][type] = "get"
libraries[jquery_ui][download][url] = "http://jquery-ui.googlecode.com/files/jquery.ui-1.6.zip"
libraries[jquery_ui][destination] = "modules/jquery_ui"
libraries[jquery_ui][directory_name] = "jquery.ui"



projects[hosting][type] = "module"
projects[hosting][download][type] = "git"
projects[hosting][download][url] = "http://git.drupal.org/sandbox/ergonlogic/1468048.git"
projects[hosting][download][branch] = "master"

projects[eldir][type] = "theme"
projects[eldir][download][type] = "git"
projects[eldir][download][url] = "http://git.drupal.org/sandbox/ergonlogic/1470888.git"
projects[eldir][download][branch] = "6.x-2.x"
projects[eldir][directory_name] = "eldir"
;projects[eldir_kt][version] = "1.0"


projects[openatria_hostmaster][type] = "profile"
projects[openatria_hostmaster][download][type] = "git"
projects[openatria_hostmaster][download][url] = "http://git.drupal.org/sandbox/ergonlogic/1467100.git"
projects[openatria_hostmaster][directory_name] = "openatria_hostmaster"
;git://github.com/ergonlogic/openatria_hostmaster.git"
projects[openatria_hostmaster][download][branch] = "master"

;libraries[profiler][download][type] = "get"
;libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-6.x-2.0-beta2.tar.gz"
;libraries[profiler][destination] = "libraries"


projects[hostmaster][type] = "profile"
projects[hostmaster][download][type] = "git"
projects[hostmaster][download][url] = "http://git.drupal.org/sandbox/ergonlogic/1226310.git"
projects[hostmaster][download][branch] = "profiler"



;Devel tools
projects[] = devel

;; Aegir contrib
;;
;; To have new Aegir contrib modules added here, please file an issue under the
;; 'Makefiles' component here: http://drupal.org/project/issues/aegir-up, using

projects[] = "hosting_notifications"
projects[] = "hosting_backup_queue"
projects[] = "hosting_backup_gc"
;;projects[] = "hosting_remote_import"
;;projects[] = "remote_import"
;hosting Drupal Gardens import: http://dirupal.org/sandbox/darthsteven/1178192
;http://drupal.org/project/aegir_feeds
projects[] = "hosting_server_titles"
;;projects[] = "hosting_platform_pathauto"
;hosting drush make sources: http://drupal.org/sandbox/darthsteven/1141164
;hosting site admin name: http://drupal.org/sandbox/darthsteven/1201092
;http://drupal.org/project/hosting_services
;http://drupal.org/project/aegir_rules
;http://drupal.org/project/hosting_site_git
;provision backup platform: http://drupal.org/sandbox/helmo/1283656
;drush remake: http://drupal.org/sandbox/darthsteven/1432518

;; ecommerce integration

projects[] = "ubercart"
projects[] = "uc_hosting"
;projects[] = "hosting_profile_roles"
projects[] = "token"


